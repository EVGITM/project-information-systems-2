<?php

use Illuminate\Support\Facades\Route;

Route::resource('customers', 'CustomerController');

Route::resource('business', 'BusinessController');

Route::resource('users', 'UserController')->middleware('auth');

Route::post('business/changerole/{bid}/{rid?}','BusinessController@changerole')->name('user.changerole')->middleware('auth');
Route::post('business/indexgal/{rid?}','BusinessController@indexgal')->name('indexgal')->middleware('auth');
Route::post('business/CancellationApproval/{bid}/{rid?}','BusinessController@CancellationApproval')->name('user.CancellationApproval')->middleware('auth');

Route::get('business/statement_by_business/{bid?}','BusinessController@sta_by')->name('statement_by_business')->middleware('auth');
Route::get('customer/short','CustomerController@short')->name('customer.short')->middleware('auth');
Route::get('customer/customer_stataments_all','BusinessController@customer_stataments_all')->name('customer.customer_stataments_all')->middleware('auth');
Route::get('all_statement','BusinessController@sta')->name('business.all_statement')->middleware('auth');

Route::get('/usersStatmenteForm','UserController@usersStatmenteForm')->name('users.usersStatmenteForm')->middleware('auth');
Route::POST('add.user', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('adduser')->middleware('auth');
#edit/delete users:
Route::get('user/deleteuser/{uid}', 'UserController@destroy')->name('user.delete')->middleware('auth');;
Route::get('user/edit_user/{cid}/{uid?}', 'UserController@editUser')->name('user.edit')->middleware('auth');;
#delete all users
Route::delete('myuserDeleteAll', 'UserController@deleteAll');

#user statements
Route::get('user/user_statement/{cid}', 'UserController@usersStatmentes')->name('user_statement')->middleware('auth');;
Route::get('myCard', 'BusinessController@myCard')->name('business.myCard')->middleware('auth');

Route::get('user/employee', 'UserController@back')->name('back')->middleware('auth');
Route::get('dashbord', 'UserController@usersDashbors')->name('users.dashbord')->middleware('auth');
Route::get('dashbordbus', 'BusinessController@businessDashbors')->name('business.dashbordbus')->middleware('auth');
Route::get('/', function () {

    return view('welcome');
})->name('root');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/downloadPDF','CustomerController@downloadPDF')->name('pdf');
Route::get('/logout', 'UserController@logout')->name('logout-home');
