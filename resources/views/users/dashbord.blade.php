<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>  

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
   

    <!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	@php
 $user= Illuminate\Support\Facades\Auth::user();
 @endphp

</head>
<body>

<div class="card text-center  mb-10 margin-1">
        <div class="card-header">
		<a href = "{{ route('logout')}}"  class="btn btn-danger btn-sm float-right" style="font-weight: bold; float:right;margin-right: 20px;">Logout</a>

        <h1 style="margin-left: 70px;">Corona Statement System</h1>
        </div>
        <div class="card-body">
        <h3 class="card-title"> Welcome {{ Auth::user()->name }}</h3>
        
		@can('check-manager') 
		<a href =  "{{url('users')}}" class="btn btn-light btn-outline-dark btn-lg">Employees List </a>

        <a href =  "{{url('customers')}}" class="btn btn-light btn-outline-dark btn-lg">Customers Health Stataments</a>
        <p><p><a href =  "{{ route('register') }}" class="btn btn-light btn-outline-dark btn-lg">Add New Employes</a>
        @endcan
 <a href =  "{{url('usersStatmenteForm')}}" class="btn btn-light btn-outline-dark btn-lg">Add Your Own Statement</a>
		<a href =  "{{ route('user_statement', $user->id) }}" class="btn btn-light btn-outline-dark btn-lg">Your Health Stataments History</a></p></p>
		<a href =  "{{url('myCard')}}" class="btn btn-light btn-outline-dark btn-lg">Buisness Info</a>

        
        <div class="box">
    <div class="container">
     	<div class="row">
			 
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                    <img src="https://i.ibb.co/fHGxMbL/2020-10-14-024856.png" alt="2020-10-14-024856" border="0"></a>
                        
						<div class="title">
							<h4>Wear a mask</h4>
						</div>
                        
						<div class="text">
							<span>Please keep wear a mask as long as you are not at your table.</span>
						</div>
                        
						                       
					 </div>
				</div>	 
				
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
					    
					    <img src="https://i.ibb.co/9WRsh1d/2020-10-14-024158.png" alt="2020-10-14-024158" border="0"></a>
                    
						<div class="title">
							<h4>Measure heat</h4>
						</div>
                        
						<div class="text">
							<span>On the business entering, you have to measure heat.
 A person who has more than 38 degrees, can not enter!</span>
						</div>
                        
						                        
					 </div>
				</div>	 
				
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                    <img src="https://i.ibb.co/wK15Rcp/2020-10-14-024133.png" alt="2020-10-14-024133" border="0"></a>
                        
						<div class="title">
							<h4>keep distance</h4>
						</div>
                        
						<div class="text">
							<span>Please be sure to keep a distance from other guests.</span>
						</div>
                        
						
					 </div>
				</div>	
				 
		
		</div>		
    </div>
</div>


</div> </body>
</html>
