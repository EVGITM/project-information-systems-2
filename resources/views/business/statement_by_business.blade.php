<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>



@php
 $user= Illuminate\Support\Facades\Auth::user();
 @endphp


   <!-- ####################################   Settings  ########################################   -->


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
Health Stataments Supervision   </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/paper-dashboard.css')}}" rel="stylesheet" />
</head>


   <!-- ####################################   Body  ########################################   -->

<body class="">
    <div class="wrapper">
        <div class="sidebar" data-color="white" data-active-color="danger">
            <div  class="logo" style='margin-left:auto; margin-right:auto'>
            <img style='position: relative;right: 5px;' src="{{URL::asset('assets/img/logo.png')}}" alt="profile Pic"  width="350">
        </div>

                                <!-- ##########   Sidebar  ############   -->

      <div style="text-align:left" class="sidebar-wrapper">
        <ul class="nav">

        @can('check-ministry') 
        <li >

            <a href="{{ route('business.index') }}">
              <i class="nc-icon nc-icon nc-bank"></i>
              <p >All Business</p>
            </a>
          </li>
          @endcan
          @can('check-manager') 

        <li  >
            <a href="{{ route('users.index') }}">
              <i class="nc-icon nc-single-02"></i>
              <p >Employees</p>
            </a>
          </li>
          @endcan

          @can('check-ministry') 

          <li>

          <a  href="{{ route('business.all_statement')}}">
              <i class="nc-icon nc-paper"></i>
              <p >All Customers Stataments</p>
            </a>
            </li>

          @endcan

          @can('check-manager') 
          <li class="active ">

            <a  href="{{ route('customers.index') }}">
              <i class="nc-icon nc-paper"></i>
              <p >Customers Stataments</p>
            </a>
          </li>
          @endcan

          <li>
            <a href="{{ route('users.usersStatmenteForm') }}">
              <i class="nc-icon nc-simple-add"></i>
              <p>Add Own Statement</p>
            </a>
          </li>

          <li>
            <a href="{{ route('user_statement', $user->id) }}">
              <i class="nc-icon nc-circle-10"></i>
              <p>Your Stataments</p>
            </a>
          </li>
          
          @can('check-both') 
          <li>
            <a href="{{ route('business.myCard') }}">
              <i class="nc-icon nc-alert-circle-i"></i>
              <p>Contact Details</p>
            </a>
          </li>
          @endcan

          
          @can('check-ministry') 
          <li>
            <a href="{{ route('business.dashbordbus') }}">
              <i class="nc-icon nc-tv-2"></i>
              <p>Back To Dashboard</p>
            </a>
          </li>
          @endcan

    
        
          
        </ul>
      </div>
    </div>


                                <!-- ##########   Main Panel  ############   -->


    <div class="main-panel" >
    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
            <div class="navbar-wrapper">
         
            @php
            @endphp
            @if (Auth::guest())


            <a class="navbar-brand" href="javascript:;"> Welcome Guest</a>
            @else

            <a class="navbar-brand" href="javascript:;"> Welcome Dear {{$user->name}}</a>
            @endif

          </div>
       
          
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
         

            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-settings-gear-65 "style="font-size: 1.3rem;"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                @guest

                  <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                  @else
                  <a class="dropdown-item" href="{{ route('logout') }}">{{ __('Logout') }}</a>
                  @endguest

                </div>

              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->




           <!-- End Navbar -->
           <div class="content" style=' max-width: 1500px; display: block; margin-left: auto; margin-right: auto;'>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
           

          <main class="py-4" style="">




    @if(Session::has('notallowed'))
    <div class = "alert alert-danger"> 
        {{Session::get('notallowed')}} 
    </div>
    @endif                    



  
    <div class="card-header">
                <h4 class="card-title"> Visited Customers Stataments for <b><u>'{{$businesses->business_name}}'</b></u> business</h4>
              </div>
              <div class="card-body">
              <br>
              <form href= "{{action('BusinessController@sta_by')}}">
                <div class="input-group">

    <div class="input-group-prepend">
  </div>
  </div>
  

        <br>
 
        <i style=' font-size: 1rem; display: inline-block;' class="nc-icon nc-zoom-split"></i>

                <input  type="text" name="search" class="input-group-text btn-sm " style='width: 200px' placeholder="Search by name or part of it" value="{{ old('search') }}">

            <div style="display: inline-block; class="col-md-6">
                <input class="btn btn-dark btn-sm btn-round " value="Click to Filter" type="submit">

            </div>
  

        </div>
    </form>
    <table class="table table-bordered" style="width:60%;  margin-left: auto; margin-right: auto; text-align: center;  ">
        @csrf

        <thead class="thead-dark">
            <tr>
            

                <th>Full Name</th>
                <th>Phone Number</th>
                <th>Time Visited </th>
                <th>Business Name</th>
           
            </tr>
            </thead>

            
<!-- the table data -->
@foreach($customers as $customer)
<tr>


    <td>{{$customer->full_name}}</td>
    <td>{{$customer->phone}}</td>
    <td>{{$customer->created_at}}</td>
    <td>{{$customer->businesses->business_name}}
    @endforeach
  
</table>








</main></div></div></div></div>
   


        
<footer class="footer" style="position: absolute; bottom: 0; width: -webkit-fill-available;">
  <div class="container-fluid">
    <div class="row">
      <nav class="footer-nav">
        <ul>
          <li><a  target="_blank">Demo Release - CORONA Supervision System </a></li>
  
        </ul>
      </nav>
      <div class="credits ml-auto">
        <span class="copyright">
          © 2020, made with <i class="fa fa-heart heart"></i> by Evgeni, Adi, Shahar and Gal
        </span>
      </div>
    </div>
  </div>
</footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="./assets/js/core/jquery.min.js"></script>
<script src="./assets/js/core/popper.min.js"></script>
<script src="./assets/js/core/bootstrap.min.js"></script>
<script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="./assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>


<script>
function SelectText(element) {
var doc = document,
  text = element,
  range, selection;
if (doc.body.createTextRange) {
  range = document.body.createTextRange();
  range.moveToElementText(text);
  range.select();
} else if (window.getSelection) {
  selection = window.getSelection();
  range = document.createRange();
  range.selectNodeContents(text);
  selection.removeAllRanges();
  selection.addRange(range);
}
}
window.onload = function() {
var iconsWrapper = document.getElementById('icons-wrapper'),
  listItems = iconsWrapper.getElementsByTagName('li');
for (var i = 0; i < listItems.length; i++) {
  listItems[i].onclick = function fun(event) {
    var selectedTagName = event.target.tagName.toLowerCase();
    if (selectedTagName == 'p' || selectedTagName == 'em') {
      SelectText(event.target);
    } else if (selectedTagName == 'input') {
      event.target.setSelectionRange(0, event.target.value.length);
    }
  }

  var beforeContentChar = window.getComputedStyle(listItems[i].getElementsByTagName('i')[0], '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, ""),
    beforeContent = beforeContentChar.charCodeAt(0).toString(16);
  var beforeContentElement = document.createElement("em");
  beforeContentElement.textContent = "\\" + beforeContent;
  listItems[i].appendChild(beforeContentElement);

  //create input element to copy/paste chart
  var charCharac = document.createElement('input');
  charCharac.setAttribute('type', 'text');
  charCharac.setAttribute('maxlength', '1');
  charCharac.setAttribute('readonly', 'true');
  charCharac.setAttribute('value', beforeContentChar);
  listItems[i].appendChild(charCharac);
}
}
</script>

</body>
</html>







