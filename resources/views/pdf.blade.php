

<!DOCTYPE html>
<html>
  <head>
  
    <meta charset="utf-8">
    <title></title>
    @php
 $user= Illuminate\Support\Facades\Auth::user();
 @endphp
    <style>
       
       table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 14px;
  text-align: left;
  border-bottom: 1px solid #ddd;
  
}

tr:hover {background-color:#f5f5f5;}

label {
  padding: 10px;
  text-align: left;
  font-weight:bold;
}
.label2 {
  padding: 80px;
  text-align: left;
  display:inline;
  font-weight:bold;
}

@font-face {
            font-family: 'iskpota';
            src: url('{{public_path()}}/fonts/your-font.ttf') format('truetype') 
        }
        body{
            font-family: 'your-font', Arimo ; 
        }

        
    </style>
  </head>
        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Generated at:') }}</label>
        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ $date}}</label>
        <br></br>
        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('By:') }}</label>
        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ $user->name}}</label>
        <br></br>
        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Business Name:') }}</label>

        <label for="phone" class="col-md-4 col-form-label text-md-right">{{$user->business->business_name}}</label>

  <div class="card">
  <h1 class="card-header text-center font-weight-bold text-uppercase py-4">Customers Health Statement </h1>
  <div class="card-body">

    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"></span>

      
            


        <table class="table table-bordered table-responsive-md table-striped text-center">
        @csrf

        <thead class="thead-dark">
       
      

            <tr>
                <th>Full Name</th>
                <th>Phone Number</th>
                <th>Time Visited </th>
           
            </tr>
            </thead>

<!-- the table data -->
@foreach($customers as $customer)
<tr>
</br>
    <td>{{$customer->full_name}}</td>
    <td>{{$customer->phone}}</td>
    <td>{{$customer->created_at->format('jS F Y    h:i')}}</td>
    
    @endforeach
    
</table>

