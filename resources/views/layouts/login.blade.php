
    <div class="main-panel" style="height: 100vh;">
    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
            <div class="navbar-wrapper">
         
            @php
            @endphp
            @if (Auth::guest())


            <a class="navbar-brand" href="javascript:;"> Welcome Guest</a>
            @else

            <a class="navbar-brand" href="javascript:;"> Welcome Dear {{$user->name}}</a>
            @endif

          </div>
       
          
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
         

            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-settings-gear-65 "style="font-size: 1.3rem;"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                @guest

                  <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                  @else
                  <a class="dropdown-item" href="{{ route('logout') }}">{{ __('Logout') }}</a>
                  @endguest

                </div>

              </li>
            </ul>
          </div>
        </div>
      </nav>