<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usersstatement extends Model
{
    public function users (){
        return $this->belongsTo('App\User','id');
    }
    public function user (){
        return $this->belongsTo('App\User','user_id');
    }
}
