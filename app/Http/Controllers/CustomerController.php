<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Business;
use App\Customer;
use App\User;
use App\Usersstatement;
use App\Role;
use PDF;

use Illuminate\Support\Carbon;
use App\Http\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create','store']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



      
    
    public function index(Request $request)
    {
        $name=$request->search;

        if( $request->start_date!=''||$request->end_date=''|| $request->searche!=''  ){
                $name=$request->search;
                $start = Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
                $end = Carbon::parse($request->end_date)->format('Y-m-d 23:59:59');
                $customers = Customer::where('business_id','=',Auth::user()->business_id)->wherebetween('created_at',[$start,$end])->where('full_name','like',"%{$name}%")->get();
                $businesses = Business::all();
                return view('customers.index',compact('customers','businesses'));
            }

        elseif(!empty($name)){ 
            $name=$request->search;
            //$start = Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
            //$end = Carbon::parse($request->end_date)->format('Y-m-d 23:59:59');
            $customers = DB::table('customers')->where('business_id','=',Auth::user()->business_id)->where('full_name','like',"%{$name}%")->get();
            $businesses = Business::all();
            return view('customers.index',compact('customers','businesses'));
            }
              else{
                $businesses = Business::all();
                  $customers = Customer::where('business_id','=',Auth::user()->business_id)->get();  
                 
                  return view('customers.index',compact('customers','businesses'));
              }        
    
    
              
    
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $customers = Customer::all();
        $businesses = Business::all();
        return view('customers.create',compact('customers','businesses'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->checkbox== TRUE){
            $customer = new Customer();
            $cus=$customer-> create($request->all());
            $cus->save();

            return view('customers.s');

            
      }else{
        return redirect()->back()->with('error', 'please confirm your health statamate');

      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function downloadPDF() {
        $date = Carbon::now(); 
        $user = Auth::user();
        $customers = Customer::where('business_id', $user->business_id)->orderBy('created_at','DESC')->get();
        $pdf = PDF::loadView('pdf', compact('customers','date'));
        
        return $pdf->download('disney.pdf');
}

}