<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Customer;
use App\City;
use App\Usersstatement;
use App\Business;
use App\Role;
use Illuminate\Support\Carbon;
use App\Http\Kernel;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response; 
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class BusinessController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create','store']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        Gate::authorize('check-ministry');
 
        if(!empty($request->all())){
            $rid=$request->rid;
            if($rid!=0){
            $businesses = Business::where('role_id',$rid)->get();
            }
            else{
                $businesses = Business::all();
            }
            $cities=City::all();
            $users = User::all();
            $roles = Role::all();}
              else{
                 
                  $businesses = Business::all();
                  $cities=City::all();
                  $users = User::all();
                  $roles = Role::all();
              }
              return view('business.index', compact('businesses','users','roles','cities'));
    
    }

  


    public function sta(Request $request)
    {
        $name=$request->search;
        if( $request->start_date!=''||$request->end_date=''|| $request->searche!=''  ){
                $start = Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
                $end = Carbon::parse($request->end_date)->format('Y-m-d 23:59:59');
                $customers = Customer::where('full_name','like',"%{$name}%")
                ->wherebetween('created_at',[$start,$end])->get();
                $businesses = Business::all();
                return view('business.all_statement',compact('customers','businesses'));
            }
        elseif(!empty($name)){ 
            $customers = Customer::where('full_name','like',"%{$name}%")
            ->get();
            $businesses = Business::all();
            return view('business.all_statement',compact('customers','businesses',));
            }
            else{
                $customers = Customer::all();  
                 $businesses = Business::all();
                 return view('business.all_statement',compact('customers','businesses',));
             }   
    }
    
   
 //   public function customer_stataments($bid)
//{ 
  //  $businesses= Business::findOrfail($bid);
   //$customers=Customer::where('business_id', $bid)->get();
    //return view('customers.index',compact('customers','businesses'));
//}

    public function changerole($bid,$rid=null){
        $business= Business::findOrfail($bid);
        $business->role_id=1;
        $business->save();
        $uid=$business->owner_id;
        $users= User::findOrfail($uid);
        $users->role_id=1;
        $users->save();
       
        //return redirect('business');
        return back();
    }

    public function CancellationApproval($bid,$rid=null){
        $business= Business::findOrfail($bid);
        $business->role_id=3;
        $business->save();
        $uid=$business->owner_id;
        $users= User::findOrfail($uid);
        $users->role_id=3;
        $users->save();
        return redirect('business');
    }

    public function customer_stataments_all(){
  
        $customers=Customer::all();
        $businesses = Business::all();
        return view('customers.index',compact ('customers','businesses'));
    }


    public function myCard()
    {        

        #$bid= Business::findOrfail($uid);

        //ask someont to make it unique
        //$user = User::findOrFail($userId);
        #$businesses = Business::all();
        $user = Auth::user();
        $business = Business::findOrFail($user->business_id);
        $oid= $business->owner_id;
        $owner = User::findOrFail($oid);

        #$bid = $user-> business_id; 
        //$business = Business::findOrFail($bid);
        #$users = User::all();
        #$roles = Role::all();        
        return view('business.card', compact('user','business','owner'));
    
    }
 
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function create()
    {
        $cities = City::all();
        return view('business.create',compact('cities'));
    }
   
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $user = User::where('email',$request->input('email'))->first();

        $user1 = Business::where('email',$request->input('owner_email'))->first();

        if($user != NULL or $user1 != NULL){
            return redirect()->back()->with('error', 'the owner email already exists, pick a new one');
        }else{

        
         User::create([
            'name' => $request['owner_name'],
            'phone' => $request['owner_phone'],
            'email' => $request['owner_email'],
           'role_id'=>3,
            'password' => Hash::make($request['password']),
            ]);

        $business = new Business();
        $bus=$business-> create($request->all());
        #$bus->password =$request->password;
        $bus->city_id = $request->city_id;
        $bus->role_id =3;
        $boid=$bus->owner_email;
        $oid = DB::table('users')->where('email', $boid)->value('id');
        $bus->owner_id =$oid;
        $bus->save();
        $bid = DB::table('businesses')->where('owner_email', $boid)->value('id');
        $update_bid = DB::table('users')->where('id', $oid)->update(['business_id' =>$bid]);

        return view('welcome'); ;
        //return ("Business registration completed successfully, you can use the system when a representative of the Ministry of Health remains your identity.");
       // return ("good work");
        //return redirect('candidates');
    }
}
  
    public function sta_by(Request $request, $bid)
    {
        $businesses= Business::findOrfail($bid);
      
       // $busname=Business::where('id',$bid)->value('business_name');
        $name=$request->search;
        if(  $request->searche!=''  ){
                //$start = Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
                //$end = Carbon::parse($request->end_date)->format('Y-m-d 23:59:59');
                $customers = Customer::where('business_id',$bid)->where('full_name','like',"%{$name}%")
               ->get();
                $businesses= Business::findOrfail($bid);
                return view('business.statement_by_business',compact('customers','businesses'));
            }
        elseif(!empty($name)){ 
            
            $customers = Customer::where('business_id',$bid)->where('full_name','like',"%{$name}%")
            ->get();
            $businesses = $businesses= Business::findOrfail($bid);
            
            return view('business.statement_by_business',compact('customers','businesses'));}


            else{
                $customers = Customer::where('business_id',$bid)->get();  
             
                 $businesses = $businesses= Business::findOrfail($bid);
                   return view('business.statement_by_business',compact('customers','businesses'));
             }                   
    
    }
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function businessDashbors()
    {
        
        $users = Auth::user();
        $bus=DB::table('businesses')->where('role_id','=', 3)->count();
        //$bus2=DB::table('businesses')->where('role_id')->count();
        //$bus3=DB::table('customers')->where('role_id')->count();
        Gate::authorize('check-ministry');
        return view('business.dashbordbus', compact('users','bus'));    
    }

    
}