<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'business_name',
        'phone',
        'city',
        'address',
        'email',
        'owner_id',

        'owner_email',

        'role_id',
        
        ];

        public function role (){
            return $this->belongsTo('App\Role','role_id');
        }
    
    //


    public function user (){
        return $this->hasMany('App\User','business_id');
    }
    
    public function city (){
    return $this->belongsTo('App\City','city_id');
    }


    public function customers (){
        return $this->hasMany('App\Customer','business_id');
    }
}
