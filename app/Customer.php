<?php

namespace App;

use Illuminate\Database\Eloquent\Model;




class Customer extends Model
{
    protected $fillable = [
        'phone', 'full_name', 'business_id',
    ];


    public function business (){
        return $this->belongsTo('App\Business','id');
    }
    
    public function businesses (){
        return $this->belongsTo('App\Business','business_id');
    }
  
}
