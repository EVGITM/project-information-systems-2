<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::define('check-ministry', function ($user) {
            return $user->isMinistryofHealth();
        });    

        Gate::define('check-manager', function ($user) {
            return $user->isManager();
        });   

        Gate::define('check-reg.employee', function ($user) {
            return $user->isEmployee();
        });   

        Gate::define('check-both', function ($user) {
            if($user->isManager() or $user->isEmployee()){
                    return TRUE;}else{
                        return FALSE;
                    }

        });

        //
    }
}
